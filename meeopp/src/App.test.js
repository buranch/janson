import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import { Provider } from 'react-redux'

configure({ adapter: new Adapter() });


it('renders without crashing', () => {
 let wrapper = shallow(
  <Provider >
    <App />
  </Provider>
 );

 expect(wrapper.exists()).toBe(true);
});
