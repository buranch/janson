
import * as types  from '../constants/actionTypes';

export function setUserData(userData) {
    return {
        type: types.SET_USER_DATA,
        userData
    };
}