const express = require('express');
const bodyparser = require('body-parser');


function createExpressApp(db) {
    const app = express();
    const User = db.collection("user");
    User.deleteMany();
    app.use(bodyparser.json());
    const headerFix = (req, res, next) => {
        // console.log(req.headers['authorization']);
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type , Authorization, Accept");
        next();
    };
    app.use(headerFix);

    app.get('/api/user', (req, res) => {

        User.find({}).toArray((err, docs) => {
            if (err) {
                return res.status(500).json({
                    error: "Error While Fetching User Data"
                })
            }
            console.log(docs);
            res.status(200).json(docs[docs.length - 1]);
        });
    });

    app.post('/api/user', (req, res) => {
        let { firstName, lastName, company, department, position, email } = req.body.body;
        let formData = {
            firstName,
            lastName,
            company,
            department,
            position,
            email
        }
        User.insertOne(formData, (err, result) => {
            if (err) {
                return res.status(500).json({
                    error: "Error While Inserting Form"
                })
            }
            console.log("Record Saved!");
            res.status(200).json({
                'user': result
            });
        })
    })
    return app;
}

module.exports = createExpressApp;